FROM codercom/code-server:v2
LABEL author="Lossless GmbH <hello@lossless.com>"

USER root

# important environment variables 
ENV NODE_VERSION_LTS="10.15.3" NODE_VERSION_STABLE="11.2.0" NVM_DIR="/usr/local/nvm"

# Set debconf to run non-interactively and install packages
RUN su -c "echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
    && apt-get update \
    && apt-get upgrade --no-install-recommends -y \
    && apt-get install -y -q --no-install-recommends \
        software-properties-common \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        g++ \
        gcc \
        git \
        make \
        openssl \
        python \
        rsync \
        ssh \
        wget \
    && apt-get update \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*"

RUN mkdir $NVM_DIR && sudo chown coder $NVM_DIR && chmod u+w $NVM_DIR

USER coder

# Install nvm with node and npm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
RUN bash -c "source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION_LTS \
    && nvm alias default $NODE_VERSION_LTS \
    && nvm use default \
    && npm config set unsafe-perm true \
    && npm install -g npm"

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION_STABLE/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION_STABLE/bin:$PATH