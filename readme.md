# @hosttoday/ht-docker-vscode
a vscode image with everything installed needed to run a vscode instance

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@hosttoday/ht-docker-vscode)
* [gitlab.com (source)](https://gitlab.com/hosttoday/ht-docker-vscode)
* [github.com (source mirror)](https://github.com/hosttoday/ht-docker-vscode)
* [docs (typedoc)](https://hosttoday.gitlab.io/ht-docker-vscode/)

## Status for master
[![build status](https://gitlab.com/hosttoday/ht-docker-vscode/badges/master/build.svg)](https://gitlab.com/hosttoday/ht-docker-vscode/commits/master)
[![coverage report](https://gitlab.com/hosttoday/ht-docker-vscode/badges/master/coverage.svg)](https://gitlab.com/hosttoday/ht-docker-vscode/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@hosttoday/ht-docker-vscode.svg)](https://www.npmjs.com/package/@hosttoday/ht-docker-vscode)
[![Known Vulnerabilities](https://snyk.io/test/npm/@hosttoday/ht-docker-vscode/badge.svg)](https://snyk.io/test/npm/@hosttoday/ht-docker-vscode)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

This image uses codercom/code-server:v2 as base. As a result you can use it largely like the base image.

```
docker run -it -p 127.0.0.1:8080:8080 -v "${HOME}/.local/share/code-server:/home/coder/.local/share/code-server" -v "$PWD:/home/coder/project" registry.gitlab.com/hosttoday/ht-docker-vscode
```

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
